[![Codeac](https://static.codeac.io/badges/3-34548324.svg "Codeac")](https://app.codeac.io/gitlab/7411/archenland)

# Archenland

Archenland is an in-development distributed renderfarm; the current implementation plan is as follows.

1. Workers connect to the server.
2. The server gets a file to render, and tells all the workers to download it.
3. The server tells the workers which frame ranges to render.
4. Each worker starts rendering, uploading the frames to the server as they complete them.
5. If one worker finishes first, it gets reassigned a fraction of the remaining frames, to act as a "load balancing" of sorts
6. Once all frames are completed, the server might even compile them into a video using ffmpeg.

Some unknowns:

Server interface: Discord bot? Website?
Client interface: A GUI is written, but the suggestion of a Blender addon is not a bad one.
