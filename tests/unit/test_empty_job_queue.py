from archenland import JobQueue

def test_new_job_queue_is_empty():
    job_queue = JobQueue()
    assert not len(job_queue)

