import pytest

from archenland import Job, JobQueue

@pytest.fixture
def job():
    return Job("fakefile.blend")

@pytest.fixture
def job_queue(job):
    job_queue = JobQueue()
    job_queue.enqueue(job)
    return job_queue

def test_job_queue_with_one_item_will_have_size_one(job_queue):
    assert len(job_queue) == 1

def test_job_queue_with_one_item_will_have_that_item(job, job_queue):
    assert job in job_queue

def test_job_queue_with_one_item_will_not_have_different_item(job_queue):
    other_job = Job("otherfile.blend")
    assert other_job not in job_queue

