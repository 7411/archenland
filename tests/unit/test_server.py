import io

import aiohttp
from aiohttp.test_utils import TestClient, TestServer
from mock import Mock
import pytest

from archenland import init_app

@pytest.mark.asyncio
async def test_when_upload_file_will_call_event_handler():
    mock_handler = Mock()
    app = init_app(mock_handler)
    async with TestClient(TestServer(app)) as client:
        with aiohttp.MultipartWriter("form-data") as mpwriter:
            mpwriter.append(
                io.BytesIO(b"yay"),
                {
                    "name" : "upload",
                    aiohttp.hdrs.CONTENT_TYPE : "application/octet-stream"
                }
            )
            await client.post("/upload", data=mpwriter)
    mock_handler.queue.assert_called()

@pytest.mark.asyncio
async def test_uploading_file_returns_200():
    mock_handler = Mock()
    app = init_app(mock_handler)
    async with TestClient(TestServer(app)) as client:
        with aiohttp.MultipartWriter("form-data") as mpwriter:
            mpwriter.append(
                io.BytesIO(b"yay"),
                {
                    "name" : "upload",
                    aiohttp.hdrs.CONTENT_TYPE : "application/octet-stream"
                }
            )
            response = await client.post("/upload", data=mpwriter)
        assert response.status == 200

@pytest.mark.asyncio
async def test_index_returns_200():
    mock_handler = Mock()
    app = init_app(mock_handler)
    async with TestClient(TestServer(app)) as client:
        response = await client.get("/")
        assert response.status == 200

@pytest.mark.asyncio
async def test_index_returns_text_html():
    mock_handler = Mock()
    app = init_app(mock_handler)
    async with TestClient(TestServer(app)) as client:
        response = await client.get("/")
        assert response.content_type == "text/html"

@pytest.mark.asyncio
async def test_server_writes_request_body_to_file():
    mock_handler = Mock()
    app = init_app(mock_handler)
    async with TestClient(TestServer(app)) as client:
        with aiohttp.MultipartWriter("form-data") as mpwriter:
            mpwriter.append(
                io.BytesIO(b"yay"),
                {
                    "name" : "upload",
                    aiohttp.hdrs.CONTENT_TYPE : "application/octet-stream"
                }
            )
            await client.post("/upload", data=mpwriter)
    _, args, _ = mock_handler.mock_calls[0]
    with open(args[0], "rb") as f:
        assert f.read() == b"yay"

