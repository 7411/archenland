from archenland import Job, JobQueue

def test_job_queue_with_two_items_will_have_size_two():
    job_queue = JobQueue()
    job_queue.enqueue(Job("fakefile.blend"))
    job_queue.enqueue(Job("fakefile2.blend"))
    assert len(job_queue) == 2

