import pytest

from archenland import JobEventHandler, JobQueue

@pytest.fixture
def job_queue():
    return JobQueue()

@pytest.fixture
def queued_job(job_queue):
    handler = JobEventHandler(job_queue)
    return handler.queue("fakefile.blend")

def test_on_new_job_event_will_add_job_to_queue(queued_job, job_queue):
    assert queued_job in job_queue

def test_new_job_will_have_correct_path(queued_job):
    assert queued_job.path == "fakefile.blend"

