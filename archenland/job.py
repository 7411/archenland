"""Implementation of rendering jobs."""

__all__ = ("Job",)

class Job:
    """Stores job data"""

    def __init__(self, path):
        self.path = path
