"""Starts the archenland webserver"""

from aiohttp import web

from .job_event_handler import JobEventHandler
from .job_queue import JobQueue
from .webserver import init_app

web.run_app(init_app(JobEventHandler(JobQueue())))
