"""Aiohttp webserver to serve archenland"""

from importlib.resources import files

from aiohttp import web

__all__ = ("init_app",)

_TMPFILE_NAME = ".tmp_job.blend"

class ArchenlandServer(web.Application):
    """Default server."""

    def __init__(self, job_handler):
        self._job_handler = job_handler
        super().__init__()

    async def serve_page(self, _):
        """Route to serve the index"""
        return web.FileResponse(files("archenland").joinpath("web/index.html"))

    async def accept_upload(self, req):
        """Route for file upload form"""
        reader = await req.multipart()
        part = await reader.next()
        filedata = await part.read()
        with open(_TMPFILE_NAME, "wb") as f:
            f.write(filedata)

        self._job_handler.queue(_TMPFILE_NAME)

        return web.Response(status=200)

def init_app(job_handler):
    """Hook to register the server."""
    app = ArchenlandServer(job_handler)
    app.add_routes([
        web.get("/", app.serve_page),
        web.post("/upload", app.accept_upload)
    ])

    return app
