"""Handler to coordinate and manage job events"""

__all__ = ("JobEventHandler",)

from .job import Job

class JobEventHandler:
    """Class to manage high level job management"""

    def __init__(self, job_queue):
        self._job_queue = job_queue

    def queue(self, path):
        """Register a new rendering job to be done"""
        job = Job(path)
        self._job_queue.enqueue(job)
        return job
