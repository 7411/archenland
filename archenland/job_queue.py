"""Container to track which files have been uploaded"""

__all__ = ("JobQueue",)

class JobQueue:
    """Simple queue"""

    def __init__(self):
        self._jobs = []

    def enqueue(self, job):
        """Add job to queue"""
        self._jobs.append(job)

    def __contains__(self, job):
        return job in self._jobs

    def __len__(self):
        return len(self._jobs)
